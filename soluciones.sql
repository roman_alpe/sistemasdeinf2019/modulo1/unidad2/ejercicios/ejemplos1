﻿USE concursos;
/*
  1- Indicar el nombre de los concursantes de Burgos*/
  
  SELECT DISTINCT c.nombre
    FROM concursantes c
    WHERE c.provincia='Burgos';
 
  SELECT DISTINCT c.nombre
    FROM concursantes
    WHERE provincia='%Burgos%'; 
 
 
 /* 2- Indicar cuantos concursantes son de Burgos*/

  SELECT COUNT(*) 
    FROM concursantes
    WHERE provincia='Burgos';
  
  /* 3- Indicar cuantos concursantes hay de cada poblacion*/
  
  SELECT DISTINCT poblacion, COUNT(*) 
    FROM concursantes
    GROUP BY poblacion;
  
  /* 4- Indicar las poblaciones que tienen concursantes de menos de 90kg*/
    
   SELECT DISTINCT poblacion 
      FROM concursantes
      WHERE peso<90;
      
  /* 5- Indicar las poblaciones que tienen más de un concursante*/
   
   /* Solución 1 */
   SELECT poblacion 
      FROM concursantes
      GROUP BY poblacion
      HAVING COUNT(*)>1; 
    
   /* Solución 2 */
   CREATE OR REPLACE VIEW c1 AS 
    SELECT c1.poblacion FROM (
    SELECT c.poblacion, COUNT(*) nConcursantes
    FROM concursantes c
    GROUP BY c.poblacion) c1
    WHERE c1.nConcursantes>1;
  
  /* 6- Indicar las poblaciones que tienen más de un concursante de menos de 90kg*/
   
   SELECT poblacion
      FROM concursantes
      WHERE peso<90
      GROUP BY poblacion
      HAVING COUNT(*)>1; 

  /** EJEMPLOS**/
  
  SELECT peso,c.altura,c.peso*c.altura/2 AS calculo
    FROM concursantes c
    WHERE (c.peso*c.altura/2)>9000;
  
  SELECT * FROM (SELECT peso,c.altura,c.peso*c.altura/2
    FROM concursantes c) c1
    WHERE c1.calculo>9000;